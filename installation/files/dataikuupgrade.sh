#!/bin/bash -x
pushd /home/dataiku/ && wget http://downloads.dataiku.com/public/studio/7.0.1/dataiku-dss-7.0.1.tar.gz
tar xzf dataiku-dss-7.0.1.tar.gz

tar -zcvf dss-`TZ="Asia/Tokyo" date +%Y%m%d`-backup.tar.gz dss
dss/bin/dss stop
echo "Y" |  dataiku-dss-7.0.1/installer.sh -d dss -u
dss/bin/dss start
