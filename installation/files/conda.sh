#!/bin/#!/usr/bin/env bash
pushd /home/dataiku/
wget https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh -O /tmp/anaconda3.sh

bash /tmp/anaconda3.sh -b -p /home/dataiku/anaconda3
rm /tmp/anaconda3.sh
echo ". /home/dataiku/anaconda3/etc/profile.d/conda.sh" >> /home/dataiku/.bashrc
bash /home/dataiku/anaconda3/etc/profile.d/conda.sh
source /home/dataiku/.bashrc

pushd /home/dataiku/
./dss/bin/dss stop
/home/dataiku/dataiku-dss-6.0.4/installer.sh -u -d dss -C
./dss/bin/dss start
