#!/bin/bash

pushd /opt/
wget https://www-eu.apache.org/dist/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz
tar -xvf spark-2.4.5-bin-hadoop2.7.tgz
ln -s /opt/spark-2.4.5-bin-hadoop2.7 /opt/spark
useradd spark
chown -R spark:spark /opt/spark*
cd /opt/spark && ./sbin/start-master.sh


sudo /opt/spark/sbin/start-slave.sh spark://$(hostname):7077
