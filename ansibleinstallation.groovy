pipeline {
	agent {
		label 'master'
	}

	stages {
		stage('ansible run') {
			steps {
				script {
						runansible()
				}
			}
		}

	}
}

def runansible() {
	echo "-- Running ansible scripts"
	sh """
  ansible-playbook setup.yml
	"""
	return
}
